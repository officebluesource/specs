#
#  To learn more about Podspec attributes see http://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|
  s.name         = "BssBaseKit"
  s.version      = "1.0.7"
  s.summary      = "BssBaseKit for easy communication with the mobile-pocket Hub"
  s.homepage     = "https://www.bluesource.at"
  s.license      = "Bluesource is the owner."
  s.author             = { "Florian Hager" => "florian.hager@bluesource.at" }
  s.platform     = :ios, "8.0"
  s.source = { :git => "ssh://git@bitbucket.org/officebluesource/bssbasekitios-framework.git", :tag => "#{s.version}"}
  s.ios.vendored_frameworks = 'BssBaseKit/Frameworks/BssBaseKit.framework'
  s.requires_arc = true
end
